Originally by Mathieu Clabaut, modified by Tuomas A.

Small daemon that lively enqueues new songs or albums in the current mpd playlist (only n songs or 1 album in advance). Album whose directory contains a file name "norandom" are always added in one time and in order. Also provides a "mpdalbumnow.py" script which completes the playlist with the end of the album of the current song.


NEW (by Tuomas A., 2014):
 - probability is determined by track/album lengths. Long tracks (or albums) have bigger probability than short ones.
