#!/usr/bin/env python
# Contributor 1:  mathieu.clabaut@gmail.com
# Contributor 2: Tuomas A. ( https://bitbucket.org/tuomas2/mpdrandomd )
# License : GPL v3

import mpdclient2, re, random, sys,os, time
from mpdclient2 import dictobj
import logging
from optparse import OptionParser
import daemon

try:
    import optcomplete
    has_optcomplete = True
except ImportError:
    has_optcomplete = False
LOG_FILENAME = '/tmp/mpdrandomd.log'
parser = OptionParser( version="0.1",
        usage="%prog [options]"
        "\n\n\tFeed an mpd daemon with a randomize playlist")
parser.add_option(  "-m", "--mode",
                    action="store", type="str", dest="mode", 
		    default="normal",
		    metavar="normal",
                    help="random mode: normal, timeweighted, timeinverse")
parser.add_option(  "-k", "--keep",
                    action="store", type="int", dest="keep", 
		    default=1000,
		    metavar="NB",
                    help="how many songs already played are keeped")
parser.add_option(  "-n", "--enqueue",
                    action="store", type="int", dest="enqueue", 
		    default=1000,
		    metavar="NB",
                    help="how many songs to enqueue before the one playing")
parser.add_option(  "--daemon",
                    action="store_const", const=True, dest="daemon", 
		    default=False,
                    help="output debug information to stderr")
parser.add_option(  "--no-update",
                    action="store_const", const=False, dest="update", 
		    default=True,
                    help="do not update the database on startup")
parser.add_option(  "--clear",
                    action="store_const", const=True, dest="clear", 
		    default=False,
                    help="emtpy mpd database on startup")
parser.add_option(  "-x", "--exclude-regex",
                    action="append", type="string", dest="exclude", 
		    default=[],
		    metavar="REGEX",
                    help="Regex against which matching files will be excluded")
parser.add_option(  "-e", "--no-equi-album",
                    action="store_const", const=True, dest="equiproba", 
		    default=True,
                    help="Does no correct the probability for album wrt individual songs")
password, host = mpdclient2.parse_host(os.environ.get('MPD_HOST', 'localhost'))
parser.add_option(  "-H", "--host",
                    action="store", type="string", dest="host", 
		    default=host,
		    metavar="NAME",
                    help="MPD host")
parser.add_option(  "-P", "--port",
                    action="store", type="int", dest="port", 
		    default=int(os.environ.get('MPD_PORT', 6600)),
		    metavar="NB",
                    help="MPD port")
parser.add_option(   "--password",
                    action="store", type="string", dest="password", 
		    default=password,
		    metavar="PWD",
                    help="MPD connexion password")
parser.add_option(  "--music-dir",
                    action="store", type="string", dest="musicdir", 
		    default=None,
		    metavar="DIR",
                    help="MPD music directory (default from mpd.conf)")
parser.add_option(  "-c","--mpd-conf",
                    action="store", type="string", dest="mpdconf", 
		    default="/home/tuma/.mpdconf",
		    metavar="FILE",
                    help="MPD conf file (default /etc/mpd.conf")
parser.set_defaults(verbose=logging.WARNING)
parser.add_option("-q", "--quiet",
                    action="store_const", const=logging.CRITICAL, dest="verbose", 
                    help="don't print status messages to stderr")
parser.add_option("-v", "--verbose",
                    action="store_const", const=logging.INFO, dest="verbose", 
                    help="output verbose status to stderr")
parser.add_option("-d", "--debug",
                    action="store_const", const=logging.DEBUG, dest="verbose", 
                    help="output debug information to stderr")
parser.add_option(  "-l","--log-file",
                    action="store", type="string", dest="logfile", 
		    default=LOG_FILENAME,
		    metavar="FILE",
                    help="log file (default %s, '-' for stderr)" % LOG_FILENAME)


if has_optcomplete:
    optcomplete.autocomplete(parser)

def loggerInit(opt):
    if opt.logfile == '-':
	logging.basicConfig(level=opt.verbose,format="%(levelname)-8s %(message)s")
    else:
	logging.basicConfig(filename = opt.logfile, level=opt.verbose,format="%(asctime)s-%(levelname)-8s %(message)s")

def Print(verb, str):
    """Print 'str' to stderr if 'verb' is below the current verbosity level"""
    if verb <= options.verbose:
        sys.stderr.write(str)


class RetryMPDConnection(mpdclient2.mpd_connection):
    def __init__(self,host,port,password):
	self.host=host
	self.port=port
	self.password=password
	mpdclient2.mpd_connection.__init__(self, host, port)
	if password != '':
	    self.password(password)
    def reconnect(self):
	mpdclient2.mpd_connection.__init__(self, self.host, self.port)


class RandomPlayList():
    def __init__(self, nb_keeped=5, nb_queued=2, doupdate=True, doclear=False,  host='localhost', port=6600, passwd='', musicdir="/home/music", mpdconf="/etc/mpd.conf", exclude=[], equiproba=True, mode = "normal"):
	logging.debug("Exclude regex : %s" % '|'.join(exclude))
	self.path_re=re.compile(r'(.*)/[^/]*')
        self.mode = mode
	self.mpdconf = mpdconf
        self.equiproba = equiproba
	self.nb_keeped = nb_keeped
	self.nb_queued = nb_queued
	self.doclear = doclear
	self.c = self.connect(host, port, passwd)
	self.musicdir=self.init_music_dir(musicdir)
        
	if doupdate:
	    self.update_db()
        if len(exclude)>0:
            self.except_re=re.compile('|'.join(exclude))
        else:
            self.except_re=re.compile(r'^$')

	#r'Enfants/|soirees/|\.m3u')

	self.c.random(0)
        logging.debug("RandomPlayList initialized")
	#self.c.play()

    def init_music_dir(self, musicdir):
	if musicdir is None:
	    re_musicdir=re.compile(r"""^\s*music_directory\s*["']([^"']*)["']""")
	    res=None
	    with open(self.mpdconf, 'r') as f: 
		for line in f:
		    res=re_musicdir.match(line)
		    if res:
			break
	    if res is not None:
		res = res.group(1)
		logging.debug("Music directory %s (from %s)" % (res,self.mpdconf))
		return  res
	else:
	    raise RuntimeError("No music directory specified or found")

    def connect(self,  host, port, passwd):
	c = None
	while c is None:
	    try:
		c = RetryMPDConnection(host,port,passwd)
	    except Exception,e:
		logging.info("Failed to connect to mpd")
		logging.debug(e)
		c = None
		time.sleep(3)
	return c


    def album_path_if_to_be_played_at_once(self, file):
        """Return the album path of 'file" if the album is to be play
        continuously (if a file called "norandom" exists in the album
        directory)
        """
	found=False
	while '/' in file and not found:
	    file=self.path_re.match(file).group(1)
	    norandompath = "%s/%s/%s" % (self.musicdir,file,"norandom")
	    found = os.path.lexists(norandompath)
	if found:
	    return file
	else:
	    return None

    def update_db(self):
	self.c.update()
	logging.info( "Updating db",)
	while 'updating_db' in self.c.status():
	    time.sleep(1)
	logging.info( "Done")

    def delete_old_songs(self, pos):
        """ Remove first playlist songs if playlist size is greater than
        nb_keeped"""
	if pos > self.nb_keeped:
	    logging.debug("Delete first %d songs" % (pos - self.nb_keeped))
	    for i in xrange(pos - self.nb_keeped, 0, -1):
		self.c.delete(i)


    def enqueue_new_songs(self, pos, length):
        """Enqueue enough songs so that there is at least nb_queued songs to be
        played ahead.  """
	if length - pos > self.nb_queued:
	    return
	to_be_added = self.nb_queued - length + pos + 1
	added = 0
	while  added < to_be_added:
	    a = self.enqueue_one_song_or_album()
	    if a != None:
                added += a
            if a == -1:
                return -1
	logging.debug("Enqued %d songs (%d required)" % (added,to_be_added))


    def get_next_random_song(self):
        """Choose a song at random.
        Be sure it is not already in the playlist and it does not match the
        'exclude' regexp.
        :returns: chosen file name
        """
	pl_files = [ x.file for x in self.c.playlistinfo() if 'file' in x]
        songs = dict([(x.file,x) for x in self.songs])
        
        for i in pl_files:
            if i in songs:
                songs.pop(i)
                continue

            folder = "/".join(i.split("/")[:-1])
            if folder in songs:
                songs.pop(folder)

        songlist = songs.values()
        if len(songlist) == 0:
            return None
        
        mode = self.mode #"normal"

        if mode == "normal":
            return songlist[random.randint(0, len(songlist)-1)]
        
        # modes timeweighted and timeinverse

        if mode == "timeinverse":
            totaltime = sum([1./int(i.time) for i in songlist])
        else:
            totaltime = sum([int(i.time) for i in songlist])

        cumulative = 0.0
        rnd = random.random()
        for k,v in songs.iteritems():
            if mode == "timeinverse":
                cumulative += 1./float(v.time)/totaltime
            else:
                cumulative += float(v.time)/totaltime
            if cumulative > rnd:
                return v
        
        return None

    def enqueue_one_song_or_album(self):
        """Enqueue a song or the song album if the song belongs to an albow
        marked as 'must not be played randomly'"""
	file = self.get_next_random_song()
        if file == None:
            return -1
        if file.foldertype:    
	    album = self.folderfiles[file.file] #  filter(lambda x: x.file.find(path)==0, self.songs)
	    for s in album:
		self.c.add(s.file)
	    return len(album)
	else:
	    logging.debug("Enqueue one song : %s" % file)
	    self.c.add(file.file)
	    return 1


    def feed_mpd(self, sleep_time=3):
        """Feed continuously mpd with new random songs.
        Wait sleep_time between two calls to enqueue_new_songs."""
	if self.doclear:
	    self.c.clear()
	self.songs = filter(lambda x: 'file' in x, self.c.listallinfo())
        self.folderfiles = folderfiles = {}
        for i in self.songs[:]:
            i.foldertype = False
            folder = "/".join(i.file.split("/")[:-1])
            if os.path.exists(self.musicdir + folder + "/norandom"):
                if folder not in folderfiles:
                    folderfiles[folder] = []
                folderfiles[folder].append(i)
                self.songs.remove(i)

        for k,v in folderfiles.iteritems():
            v.sort(key=lambda x: x.file)
            totaltime = sum([int(i.time) for i in v])
            do = dictobj(file=k, foldertype=True,
                                 time = totaltime)
            self.songs.append(do)
            for i in v:
                i.directoryobject = do

	wasplaying = self.c.status().state == 'play'

	while True:
	    try:
                playlist_len = int(self.c.status().playlistlength)
            except KeyError:
                print "Trying to reconnect"
                self.c.reconnect()
                playlist_len = int(self.c.status().playlistlength)
            #print playlist_len
            #raise Exception
	    if playlist_len == 0:
		self.enqueue_new_songs(0,0)
		playlist_len = int(self.c.status().playlistlength)
	    if 'song' in  self.c.status():
		pos = int(self.c.status().song)
	    else:
		pos = 0
	    self.delete_old_songs(pos)
            
            
            #enable crossfade in all other cases than when we have adjacent songs
            status = self.c.status()
            
            song = self.c.playlistid(int(status.songid))[0]
            nextsong = self.c.playlistid(int(status.nextsongid))[0]

            if song.album == nextsong.album and song.artist == nextsong.artist and int(nextsong.pos) == int(song.pos) + 1:
               self.c.crossfade(0)
            else:
                self.c.crossfade(5)

	    self.enqueue_new_songs(pos,playlist_len)
	    #curlen = int(self.c.currentsong().time)
	    #curtime = int(self.c.status().time.split(':')[0])
	    #sleep(curlen-curtime + 3)
	    time.sleep(sleep_time)

		



class UsageError(Exception):
    def __init__(self, msg):
        self.msg = msg

def main(argv=None):
    global options
    if argv is None:
        argv = sys.argv
    (options, args) = parser.parse_args()
    if options.daemon:
	daemon.createDaemon()
    loggerInit(options)
    while True:
        logging.debug("Creating RandomPlayList")
	r=RandomPlayList(doupdate=options.update, doclear=options.clear,
		nb_keeped=options.keep, nb_queued=options.enqueue,
		host=options.host, passwd=options.password,
		port=options.port, mpdconf=options.mpdconf,
		musicdir=options.musicdir, exclude=options.exclude,
                equiproba=options.equiproba, mode = options.mode)
	r.feed_mpd()

if __name__ == "__main__":
    sys.exit(main())
# vim: se sw=4 sts=4 et:
