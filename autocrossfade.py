#!/usr/bin/env python
# Contributor 1:  mathieu.clabaut@gmail.com
# Contributor 2: Tuomas A. ( https://bitbucket.org/tuomas2/mpdrandomd )
# License : GPL v3

import mpdclient2, re, random, sys,os, time
from mpdclient2 import dictobj
import logging
from optparse import OptionParser
import daemon

class RetryMPDConnection(mpdclient2.mpd_connection):
    def __init__(self,host,port,password):
	self.host=host
	self.port=port
	self.password=password
	mpdclient2.mpd_connection.__init__(self, host, port)
	if password != '':
	    self.password(password)
    def reconnect(self):
	mpdclient2.mpd_connection.__init__(self, self.host, self.port)


def main():
    c = RetryMPDConnection("localhost",6600,"")

    while True:
        #enable crossfade in all other cases than when we have adjacent songs
        status = c.status()
        
        song = c.playlistid(int(status.songid))[0]
        nextsong = c.playlistid(int(status.nextsongid))[0]

        if song.album == nextsong.album and song.artist == nextsong.artist and int(nextsong.pos) == int(song.pos) + 1:
            c.crossfade(0)
        else:
            c.crossfade(5)

	time.sleep(15)

if __name__ == "__main__":
    sys.exit(main())
# vim: se sw=4 sts=4 et:
